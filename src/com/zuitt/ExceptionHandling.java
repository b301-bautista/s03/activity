package com.zuitt;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");
        Scanner in = new Scanner(System.in);

        try {
            int num = in.nextInt();
            int answer = 1;
            int counter = 1;

            // Using while loop
            while (counter <= num) {
                answer *= counter;
                counter++;
            }

            // Factorial calculation for zero or negative numbers
            if (num == 0) {
                answer = 1;
            } else if (num < 0) {
                System.out.println("Cannot compute factorials of a negative numbers...");
                return;
            }
            // while loop
            System.out.println("The factorial of " + num +  " is "  + answer);

            // Using for loop
            answer = 1;
            for (int i = 1; i <= num; i++) {
                answer *= i;
            }
            // for loop
            System.out.println("The factorial of " + num +  " is "  + answer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
