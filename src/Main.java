public class Main {
    public static void main(String[] args) {

        // Section A - Loops
        // For loops
        // i=0 initial/starting value
        // i<10 - condition to be satisfied
        // i++ - change of value
        for(int i=0; i<10; i++){
            System.out.println("Current count: " + i);
        }

        int[] hundreds = {100, 200, 300, 400, 500};

        for(int i=0; i < hundreds.length; i++){
            // 0, true, hundreds[0] = 100, i++ >> 1
            // 1, true, hundreds[1] = 200, i++ >> 2
            // 2, true, hundreds[2] = 300, i++ >> 3
            // 3, true, hundreds[3] = 400, i++ >> 4
            // 4, true, hundreds[4] = 500, i++ >> 5
            // 5, false >> the loop will stop
            System.out.println(hundreds[i]);
        }

        System.out.println("Other approach ----");
        for(int hundred: hundreds){
            System.out.println(hundred);
        }


//        for(int i=0; i<10; i++){
//            System.out.println("Current count: " + i);
//        }


        // While
        int i = 0;
        while (i < 10){
            System.out.println("Current count: " + i);
            i++;
        }

        // Do while - Runs atleast once
        int y = 0;
        do {
            System.out.println("Y value: "+ y);
        }
        while(y != 0);






    }
}
